from django.db import models
from django.core.validators import MinValueValidator


class Rate(models.Model):
    date = models.DateField()
    rate_value = models.FloatField(validators=[MinValueValidator(0.0)])

    class Meta:
        ordering = ['-date']
