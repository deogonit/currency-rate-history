from django.shortcuts import render, redirect
from django.views import View

import time
import json

from .forms import CreateRateForm
from .models import Rate


class CreateRateView(View):
    template_name = 'history_rate/create_rate.html'

    def get(self, request, *args, **kwargs):
        form = CreateRateForm()
        rates = Rate.objects.all()[:10]
        context = {
            'form': form,
            'rates': rates
        }
        return render(request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        form = CreateRateForm(request.POST)
        if form.is_valid():
            rate = form.save(commit=False)
            date = request.POST['date']
            rates = Rate.objects.filter(date=date)

            if rates.exists():
                rate = rates[0]
                rate.rate_value = request.POST['rate_value']

            rate.save()
            return redirect('history_rate')
        rates = Rate.objects.all()[:10]
        context = {
            'form': form,
            'rates': rates
        }

        return render(request, self.template_name, context=context)


def convert_to_unix_time(date):
    return int(time.mktime(date.timetuple())) * 1000


class HistoryRateView(View):
    template_name = 'history_rate/currency_rate.html'

    def get(self, request, *args, **kwargs):
        rates = Rate.objects.all()
        data = []
        for rate in rates:
            data.append([convert_to_unix_time(rate.date), rate.rate_value])
        is_empty = True if len(data) else False
        context = {
            'data': json.dumps(data),
            'is_empty': is_empty
        }
        return render(request, self.template_name, context=context)
