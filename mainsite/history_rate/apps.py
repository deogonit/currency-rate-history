from django.apps import AppConfig


class HistoryRateConfig(AppConfig):
    name = 'history_rate'
