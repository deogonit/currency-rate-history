from django.urls import path
from .views import CreateRateView, HistoryRateView

urlpatterns = [
    path('create-rate/', CreateRateView.as_view(), name='create_rate'),
    path('', HistoryRateView.as_view(), name='history_rate')
]
