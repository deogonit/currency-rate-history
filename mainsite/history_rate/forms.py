from django import forms
from .models import Rate
import datetime


def present_or_past_date(value):
    if value > datetime.date.today():
        raise forms.ValidationError("The date can\'t be in the future")
    return value


class CreateRateForm(forms.ModelForm):
    rate_value = forms.FloatField(
        required=True,
        widget=forms.NumberInput(
            attrs={'class': 'form-control'}),
        min_value=0.0)
    date = forms.DateField(
        validators=[present_or_past_date],
        required=True,
        initial=datetime.date.today,
        widget=forms.DateInput(
            attrs={
                'type': 'date',
                'class': 'form-control'
            })
    )

    class Meta:
        model = Rate
        fields = [
            'date',
            'rate_value'
        ]
